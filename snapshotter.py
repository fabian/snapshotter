from datetime import datetime
import os
import subprocess
import urllib2

from boto.ec2.connection import EC2Connection
from boto.ec2.regioninfo import RegionInfo
from boto.exception import EC2ResponseError
try:
    import snapshotter_conf as conf
except ImportError:
    raise RuntimeError('snapshotter_conf.py expected but missing')


AWS_KEY = getattr(conf, 'AWS_KEY', None)
AWS_SECRET = getattr(conf, 'AWS_SECRET', None)
MYSQL_HOST = getattr(conf, 'MYSQL_HOST', 'localhost')
MYSQL_SOCKET = getattr(conf, 'MYSQL_SOCKET', '/var/run/mysqld/mysqld.sock')
MYSQL_USERNAME = getattr(conf, 'MYSQL_USERNAME', None)
MYSQL_PASSWORD = getattr(conf, 'MYSQL_PASSWORD', None)
PREFIX = getattr(conf, 'PREFIX', 'snapshotter')
REGION = getattr(conf, 'REGION', 'us-east-1')
HOUR_OF_CRON_EXECUTION = getattr(conf, 'HOUR_OF_CRON_EXECUTION', 0)


class Snapshotter(object):
    """
    Snapshotter is a frontend for the `ec2-consistent-snapshot` command line
    tool. It creates snapshots of all EBS volumes that are currently attached
    to the instance and give them custom descriptions that are easy to parse
    and keep track of.
    """
    def __init__(self, aws_key, aws_secret, mysql_username, mysql_password,
        mysql_host, mysql_socket, prefix, region, mountpoint='/'):
        self.aws_key = aws_key
        self.aws_secret = aws_secret
        self.mysql_username = mysql_username
        self.mysql_password = mysql_password
        self.mysql_host = mysql_host
        self.mysql_socket = mysql_socket
        self.prefix = prefix
        self.region = region
        self.mountpoint = mountpoint
        self.instance_id = self._get_instance_id()
        self.instance = self._get_instance_by_id(self.instance_id)
        self.volume_ids = self._get_volume_ids(self.instance)

    def _get_region_info(self):
        return RegionInfo(endpoint='%s.ec2.amazonaws.com' % self.region,
            name=self.region)

    def _get_instance_id(self):
        url = 'http://169.254.169.254/latest/meta-data/instance-id/'
        response = urllib2.urlopen(url)
        return response.read()

    def _get_instance_by_id(self, instance_id):
        region_info = self._get_region_info()
        conn = EC2Connection(self.aws_key, self.aws_secret, region=region_info)
        for reservation in conn.get_all_instances([instance_id]):
            for instance in reservation.instances:
                if instance.id == instance_id:
                    return instance

    def _get_volume_ids(self, instance):
        return [b.volume_id for b in instance.block_device_mapping.values()]

    def delete_dispensable_snapshots(self):
        """
        Deletes snapshots that are not required anymore and keeps:
            * All within the last 24 hours
            * One every day within the last 7 days
            * One every week within the last 31 days
            * One every month within the last year
        """
        region_info = self._get_region_info()
        c = EC2Connection(self.aws_key, self.aws_secret, region=region_info)
        snapshots = [s for s in c.get_all_snapshots(owner='self') if \
            s.description.startswith('%s:' % self.prefix)]
        for s in snapshots:
            now = datetime.now()
            dt = datetime(*map(int, s.description.split(':')[1:6]))
            delta = (now - dt)
            if delta.days == 0:
                continue  # all for 24 hours
            elif delta.days < 8:
                if dt.hour == HOUR_OF_CRON_EXECUTION:
                    continue  # first per day for last week
            elif delta.days < 32:
                if dt.hour == HOUR_OF_CRON_EXECUTION and dt.weekday() == 0:
                    continue  # first per week for last month
            elif delta.days < 365:
                if dt.day < 8 and dt.hour == HOUR_OF_CRON_EXECUTION and \
                    dt.weekday() == 0:
                    continue  # first per month for last year
            try:
                s.delete()
                print 'Old snapshot deleted (Description: %s)' % s.description
            except EC2ResponseError, exc:
                print exc  # fail silently if e.g. mounted

    def create_snapshots(self):
        """
        Create snapshots of all EBS volumes that are currently attached.
        The description property of the snapshot include all the following
        data separated by colons (in that order):
            * Prefix (String)
            * Year (int)
            * Month (int)
            * Day (int)
            * Hour (int)
            * Minute (int)
            * Day of the week (int)
            * Day of the year (int)
            * Days since 1.1.1970 (int)
            * Volume ID of the Snapshot (String)
        """
        for volume_id in self.volume_ids:
            now = datetime.now()
            description = '%(prefix)s:%(year)d:%(month)d:%(day)d:%(hour)d:'\
                '%(minute)d:%(weekday)d:%(day_of_year)d:'\
                '%(days_since_epoche)d:%(volume_id)s' % {
                'year': now.year,
                'month': now.month,
                'day': now.day,
                'hour': now.hour,
                'minute': now.minute,
                'weekday': now.weekday(),
                'day_of_year': now.timetuple().tm_yday,
                'days_since_epoche': (now - datetime(1970, 1, 1)).days,
                'volume_id': volume_id,
                'prefix': self.prefix,
            }

            mysql_option = ''
            if all([self.mysql_host, self.mysql_socket, self.mysql_username,
                self.mysql_password]):
                mysql_option = '--mysql --mysql-host "%(mysql_host)s" '\
                    '--mysql-socket "%(mysql_socket)s" '\
                    '--mysql-username "%(mysql_username)s" '\
                    '--mysql-password "%(mysql_password)s"' % {
                    'mysql_host': self.mysql_host,
                    'mysql_socket': self.mysql_socket,
                    'mysql_username': self.mysql_username,
                    'mysql_password': self.mysql_password,
                }

            cmd = 'ec2-consistent-snapshot --aws-access-key-id="%(aws_key)s" '\
                '--aws-secret-access-key="%(aws_secret)s" '\
                '--xfs-filesystem "%(mountpoint)s" --region="%(region)s" '\
                '%(mysql_option)s --description="%(description)s" '\
                '"%(volume_id)s"' % {
                'aws_key': self.aws_key,
                'aws_secret': self.aws_secret,
                'mountpoint': self.mountpoint,
                'region': self.region,
                'mysql_option': mysql_option,
                'description': description,
                'volume_id': volume_id,
            }
            ret_value = subprocess.call(cmd, shell=True,
                stdout=open(os.devnull, 'w'), stderr=open(os.devnull, 'w'))
            if ret_value == os.EX_OK:
                print 'Snapshot initialized (Description: %s)' % description
            else:
                print 'Failed with exit status: %d' % ret_value


if __name__ == '__main__':
    s = Snapshotter(aws_key=AWS_KEY, aws_secret=AWS_SECRET,
        mysql_username=MYSQL_USERNAME, mysql_password=MYSQL_PASSWORD,
        mysql_host=MYSQL_HOST, mysql_socket=MYSQL_SOCKET,
        prefix=PREFIX, region=REGION, mountpoint='/')
    s.create_snapshots()
    s.delete_dispensable_snapshots()
